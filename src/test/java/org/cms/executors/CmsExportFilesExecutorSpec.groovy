package org.cms.executors

import org.cms.executors.dto.JsonDocument
import spock.lang.Specification

import java.util.concurrent.Executors

class CmsExportFilesExecutorSpec extends Specification {
    CmsExportFilesExecutor cmsExportFilesExecutor
    CmsClient client
    def cmsFileWriter
    def executorService
    def jsonDocuments

    def setup() {
        client = Mock(CmsClient)
        cmsFileWriter = Mock(CmsFileWriter)
        executorService = Executors.newFixedThreadPool(10)
        cmsExportFilesExecutor = new CmsExportFilesExecutor(client, cmsFileWriter, executorService)
    }

    def populateRandomDocuments() {
        return (1..1000).collect {
            UUID.randomUUID().toString()
        }
    }

    def "test initializing the executor"() {
        given: "json documents"
        jsonDocuments = populateRandomDocuments()

        when: "executor is initialized"
        cmsExportFilesExecutor.initiateFetchAndExportProcess()

        then:
        1 * client.retrieveDocuments() >> {
//            Thread.sleep(1000)
            return jsonDocuments
        }
        1000 * client.retrieveSingleDocument(_ as String) >> Mock(JsonDocument)
        1000 * cmsFileWriter.writeDocument(_ as JsonDocument)
        0 * _
    }
}
