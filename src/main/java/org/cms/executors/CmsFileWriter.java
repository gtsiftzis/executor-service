package org.cms.executors;

import java.io.FileWriter;
import java.io.IOException;

import org.cms.executors.dto.JsonDocument;
import org.cms.executors.dto.JsonDocumentWriteResponse;

class CmsFileWriter {
    private static final String OUTPUT_PATH = "/tmp/json/%s";
    private static final int SLEEP_IN_MILLIS = 1000;

    JsonDocumentWriteResponse writeDocument(JsonDocument jsonDocument) {
        System.out.println("writing document " + jsonDocument);
        FileWriter fileWriter;

        JsonDocumentWriteResponse jsonDocumentWriteResponse = new JsonDocumentWriteResponse();
        try {
            Thread.sleep(SLEEP_IN_MILLIS);
            fileWriter = new FileWriter(String.format(OUTPUT_PATH, jsonDocument.getId()));
            fileWriter.write(jsonDocument.getContent());
            fileWriter.flush();
        } catch (IOException | InterruptedException e) {
            jsonDocumentWriteResponse.setStatus("FAILED");
            return jsonDocumentWriteResponse;
        }


        jsonDocumentWriteResponse.setWrittenDocument(jsonDocument);
        jsonDocumentWriteResponse.setStatus("OK");

        return jsonDocumentWriteResponse;
    }
}
