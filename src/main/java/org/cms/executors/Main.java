package org.cms.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static final int POOL_COUNT = 200;

    public static void main(String[] args) throws InterruptedException {
        CmsClient cmsClient = new CmsClient();
        CmsFileWriter cmsFileWriter = new CmsFileWriter();
        ExecutorService executorService = Executors.newFixedThreadPool(POOL_COUNT);

        CmsExportFilesExecutor executor = new CmsExportFilesExecutor(cmsClient, cmsFileWriter, executorService);
        executor.initiateFetchAndExportProcess();
    }
}
