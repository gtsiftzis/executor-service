package org.cms.executors;

import com.google.gson.Gson;
import org.cms.executors.dto.CmsDocument;
import org.cms.executors.dto.JsonDocument;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class CmsClient {
    private static final String CMS_SITE_API = "http://localhost:8080/site/api/ctplotteryiecms/documents";
    private static final String OFFSET_QUERY = "?_offset=%d";

    JsonDocument retrieveSingleDocument(String documentUrl) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.setId(documentUrl.substring(documentUrl.lastIndexOf("/"), documentUrl.length()) + ".json");
        InputStreamReader reader = null;
        try {
            final URL url = new URL(documentUrl);
            reader = new InputStreamReader(url.openStream());
            BufferedReader bufferedReader = new BufferedReader(reader);
            populateJsonDocument(jsonDocument, bufferedReader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonDocument;
    }

    private void populateJsonDocument(JsonDocument jsonDocument, BufferedReader bufferedReader) {
        jsonDocument.setContent(new Gson().toJson(bufferedReader
                .lines().collect(Collectors.joining("\n"))));
    }

    List<String> retrieveDocuments() {
        List<String> jsonDocuments = new ArrayList<>();

        try {
            getDocumentsWithOffset(jsonDocuments, 0);
            getDocumentsWithOffset(jsonDocuments, 100);
            getDocumentsWithOffset(jsonDocuments, 200);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonDocuments;
    }

    private void getDocumentsWithOffset(List<String> jsonDocuments, int offset) throws IOException {
        final URL url = new URL(CMS_SITE_API + String.format(OFFSET_QUERY, offset));
        InputStreamReader reader = new InputStreamReader(url.openStream());
        CmsDocument cmsDocument = new Gson().fromJson(reader, CmsDocument.class);
        jsonDocuments.addAll(Arrays.stream(cmsDocument.getItems()).filter(Objects::nonNull).map(document -> document.getLink().getUrl()).collect(Collectors.toList()));
        reader.close();
    }
}
