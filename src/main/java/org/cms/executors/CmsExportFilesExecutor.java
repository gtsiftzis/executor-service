package org.cms.executors;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.cms.executors.dto.JsonDocumentWriteResponse;

class CmsExportFilesExecutor {
    private CmsClient client;
    private CmsFileWriter cmsFileWriter;
    private ExecutorService executorService;

    CmsExportFilesExecutor(CmsClient client, CmsFileWriter cmsFileWriter, ExecutorService executorService) {
        this.client = client;
        this.cmsFileWriter = cmsFileWriter;
        this.executorService = executorService;
    }

    void initiateFetchAndExportProcess() throws InterruptedException {
        logToConsole("initializing executor");

        final long timeStarted = System.nanoTime();

        List<String> urls = client.retrieveDocuments();

        List<CompletableFuture<JsonDocumentWriteResponse>> documentContent = urls.stream()
            .map(url -> CompletableFuture
                .supplyAsync(() -> client.retrieveSingleDocument(url), executorService)
                .thenApply(cmsFileWriter::writeDocument).exceptionally(throwable -> {
                    JsonDocumentWriteResponse response = new JsonDocumentWriteResponse();
                    response.setStatus("FAILED");
                    return response;
                }))
            .collect(Collectors.toList());

        logToConsole(String.format("collected %s documents", documentContent.size()));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.HOURS);

        long time = System.nanoTime() - timeStarted;

        logToConsole(String.format("Tasks took %.3f s to run%n", time / (1e6 * 1000)));
        logToConsole("executor terminated");
    }

    private void logToConsole(String message) {
        System.out.println(message);
    }
}
