package org.cms.executors.dto;

public class JsonDocumentWriteResponse {
    private String status;
    private JsonDocument writtenDocument;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JsonDocument getWrittenDocument() {
        return writtenDocument;
    }

    public void setWrittenDocument(JsonDocument writtenDocument) {
        this.writtenDocument = writtenDocument;
    }
}
