package org.cms.executors.dto;

public class CmsDocument {
    private Item[] items;

    public Item[] getItems() {
        return items != null ? items : new Item[0];
    }

    public void setItems(Item[] items) {
        this.items = items;
    }
}
